import datetime
import pandas as pd
import numpy as np
from flask import Flask, render_template, request, Response, jsonify, make_response, send_file
import matplotlib
#matplotlib.use('TkAgg')
import matplotlib.pyplot as plt
plt.style.use('fivethirtyeight')

JSON_MIME_TYPE = 'application/json'

app = Flask(__name__)

@app.route('/dg', methods=["GET","POST"])
def dg():
    data = request.json

    action = request.args.get('action')
    foodItem = request.args.get('foodItem')


    result = run_random_forest_classification_diet()

    health_status = "wrong"

    if("health: 1" in result):
        health_status = "right"

    host = "https://heath-a-thon.ew.r.appspot.com"
    #host = "http://localhost:8080"
    g_response = {
        "fulfillmentMessages": [
            {
                "text": {
                    "text": [
                        "Out of your last 100 meals, 38 of them are healthy and 62 are unhealthy. While " + foodItem + " was unhealthy, last week we were tracking at 40 unhealthy meals out of 100. We are moving in the "+health_status+" direction. Shall we plan your next healthy meal?"
                    ],
                    "url": "food_history.png",
                    "action": action,
                    "foodItem": foodItem
                }
            }
         ]

        }

    return make_response(jsonify(g_response))

@app.route('/get_image')
def get_image():
    image = request.args.get('image')
    return send_file(''.join(["static/",image]), mimetype='image/gif')

def run_random_forest_classification_diet():
    dataset = pd.read_csv("data/diet_dataset_1.csv")
    # We just want to include Age and Estimated Salary as the independent variables
    x = dataset.iloc[:, 1:16].values
    y = dataset.iloc[:, 19].values

    # SPLITTING THE DATASET INTO TRAINING SET AND TEST SET
    from sklearn.model_selection import train_test_split
    # Divding data set into 80% Training and 20% Test data, set test_size = 0.2, set random state as 0 for no randomness
    x_train, x_test, y_train, y_test = train_test_split(x, y, test_size=0.25, random_state=0)
    # Let's check the training sets and test sets
    x_train, x_test, y_train, y_test

    # FEATURE SCALING
    # In the given dataset, we have numerical values for age and salary
    # Since the scale of salary is much higher than age, in ML equations, using it directly will give a bias to salary
    # In order to given age it's due importance we need to scale different variables
    # Scaling can be done using methods like Standardisation and Normalisation
    # COMMENTED as most of the ML libraries have in-built feature scaling
    from sklearn.preprocessing import StandardScaler
    sc_x = StandardScaler()
    x_train = sc_x.fit_transform(x_train)
    x_test = sc_x.fit_transform(x_test)

    # Fitting Random Forest Classification to our training set
    from sklearn.ensemble import RandomForestClassifier
    classifier = RandomForestClassifier(n_estimators=5, criterion='entropy', random_state=0)
    classifier.fit(x_train, y_train)

    # Predicting the Test set results
    y_pred = classifier.predict(x_test)


    # Making the confusion matrix
    from sklearn.metrics import confusion_matrix
    cm = confusion_matrix(y_test, y_pred)
    tp = cm[0][0]
    tn = cm[1][1]
    fp = cm[0][1]
    fn = cm[1][0]

    new_input = [[5,3,4,2,2,10,5,10,14,1,2,2,2,18,20]]

    dataset_user = pd.read_csv("data/diet_dataset_user.csv")
    # We just want to include Age and Estimated Salary as the independent variables

  #  plot = dataset_user.plot.bar()
  #  fig = plot.get_figure()
  #  fig.savefig("static/output.png")


    #index = np.arange(len(dataset_user.iloc[:,0]))
    #plt.bar(index, dataset_user.iloc[:,1])
    #plt.xlabel('Food', fontsize=5)
    #plt.ylabel('Count', fontsize=5)
    #plt.xticks(index, dataset_user.iloc[:,0], fontsize=5, rotation=30)
    #plt.title('Food history')

    #ax = (dataset_user.plot(kind='bar', x= 'food', y='count', legend=None))
    #ax.xaxis.set_label_text("Food Item")
    #ax.yaxis.set_label_text("Food Item count")
    #ax.title('')
    #plt.tight_layout()


    #plt.savefig('static/food_history.png')

    new_output = classifier.predict(new_input)

    accuracy = (tp + tn)/(tp + tn + fp + fn)
    precision = tp / (tp + fp)
    recall = tp / (tp + fn)
    f1_score = (2 * precision * recall)/(precision + recall)

    return ''.join(["health: ",str(new_output),"Accuracy: ", str(accuracy), " Precision: ", str(precision), " recall: ", str(recall), " f1 score:  ", str(f1_score)])



def run_random_forest_classification_food():
    dataset = pd.read_csv("data/good-bad-food.csv")
    # We just want to include Age and Estimated Salary as the independent variables
    x = dataset.iloc[:, 3:35].values
    y = dataset.iloc[:, 1].values

    # SPLITTING THE DATASET INTO TRAINING SET AND TEST SET
    from sklearn.model_selection import train_test_split
    # Divding data set into 80% Training and 20% Test data, set test_size = 0.2, set random state as 0 for no randomness
    x_train, x_test, y_train, y_test = train_test_split(x, y, test_size=0.25, random_state=0)
    # Let's check the training sets and test sets
    x_train, x_test, y_train, y_test

    # FEATURE SCALING
    # In the given dataset, we have numerical values for age and salary
    # Since the scale of salary is much higher than age, in ML equations, using it directly will give a bias to salary
    # In order to given age it's due importance we need to scale different variables
    # Scaling can be done using methods like Standardisation and Normalisation
    # COMMENTED as most of the ML libraries have in-built feature scaling
    from sklearn.preprocessing import StandardScaler
    sc_x = StandardScaler()
    x_train = sc_x.fit_transform(x_train)
    x_test = sc_x.fit_transform(x_test)

    # Fitting Random Forest Classification to our training set
    from sklearn.ensemble import RandomForestClassifier
    classifier = RandomForestClassifier(n_estimators=30, criterion='entropy', random_state=0)
    classifier.fit(x_train, y_train)

    # Predicting the Test set results
    y_pred = classifier.predict(x_test)

    new_input = [[5.97,0.661,0,0.046,0.252,0.88316,0.13077,0.0262,0,0,0.00017,0.04,0,0,0,0,0.00195,0.00205,0.0199,0.51,0.00171,0,0.43,0.003,0.0207,0.36,0.00363,0.8,2.08,0.024,2.9,0.006]]

    new_output = classifier.predict(new_input)
    # Making the confusion matrix
    from sklearn.metrics import confusion_matrix
    cm = confusion_matrix(y_test, y_pred)
    tp = cm[0][0]
    fp = cm[1][1]
    tn = cm[0][1]
    fn = cm[1][0]

    new_input = [
        [5.97, 0.661, 0, 0.046, 0.252, 0.88316, 0.13077, 0.0262, 0, 0, 0.00017, 0.04, 0, 0, 0, 0, 0.00195, 0.00205,
         0.0199, 0.51, 0.00171, 0, 0.43, 0.003, 0.0207, 0.36, 0.00363, 0.8, 2.08, 0.024, 2.9, 0.006]]

    new_output = classifier.predict(new_input)

    accuracy = (tp + tn)/(tp + tn + fp + fn)
    precision = tp / (tp + fp)
    recall = tp / (tp + fn)
    f1_score = (2 * precision * recall)/(precision + recall)

    return ''.join(["New Output",str(new_output),"Accuracy: ", str(accuracy), " Precision: ", str(precision), " recall: ", str(recall), " f1 score:  ", str(f1_score)])





@app.route('/')
def root():
    # For the sake of example, use static information to inflate the template.
    # This will be replaced with real information in later steps.
    dummy_times = [datetime.datetime(2018, 1, 1, 10, 0, 0),
                   datetime.datetime(2018, 1, 2, 10, 30, 0),
                   datetime.datetime(2018, 1, 3, 11, 0, 0),
                   ]

    return render_template('index.html', times=dummy_times)



if __name__ == '__main__':
    # This is used when running locally only. When deploying to Google App
    # Engine, a webserver process such as Gunicorn will serve the app. This
    # can be configured by adding an `entrypoint` to app.yaml.
    # Flask's development server will automatically serve static files in
    # the "static" directory. See:
    # http://flask.pocoo.org/docs/1.0/quickstart/#static-files. Once deployed,
    # App Engine itself will serve those files as configured in app.yaml.
    app.run(host='127.0.0.1', port=8080, debug=True)