# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I run the app? ###


python3 -m venv env
source env/bin/activate


cd health-a-thon-ml
pip install  -r requirements.txt

python main.py

http://localhost:8080

To deploy:
  $ gcloud app deploy --promote --stop-previous-version

You can stream logs from the command line by running:
  $ gcloud app logs tail -s default

To view your application in the web browser run:
  $ gcloud app browse


### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact